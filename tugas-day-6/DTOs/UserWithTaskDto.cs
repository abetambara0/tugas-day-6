﻿namespace tugas_day_6.DTOs
{
    public class UserWithTaskDto
    {
        public List<TaskOutputDto> tasks = new List<TaskOutputDto>();
        public int pk_user_id { get; set; }
        public string name { get; set; } = string.Empty;

    }
}
