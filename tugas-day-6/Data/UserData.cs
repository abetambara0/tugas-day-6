﻿using System.Data.SqlClient;
using tugas_day_6.Model;

namespace tugas_day_6.Data
{
    public class UserData
    {

        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;
        public UserData(IConfiguration configuration)
        {
            _configuration = configuration;
            this.ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public List<User> GetAll()
        {
            List<User> users = new List<User>();

            string query = " SELECT * FROM Users ";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new User
                                {
                                    pk_user_id = Convert.ToInt32(reader["pk_user_id"]),
                                    name = reader["name"].ToString() ?? string.Empty
                                }); ;
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return users;
        }

        public List<User> GetUsersByName(string name)
        {
            List<User> users = new List<User>();
            string query = "Select * FROM Users WHERE name=@name";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        connection.Open();
                        command.Parameters.AddWithValue("@name", name);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new User
                                {
                                    pk_user_id = Convert.ToInt32(reader["pk_user_id"]),
                                    name = reader["name"].ToString() ?? string.Empty
                                });
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return users;

        }

        public int Insert(User user)
        {
            bool result = false;
            int lastValue = 0;
            string query = $"INSERT INTO Users (name) VALUES ('{user.name}')";
            string query2 = $"SELECT IDENT_CURRENT ('Users')";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        result = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
                using (SqlCommand command = new SqlCommand(query2, connection))
                {
                    try
                    {
                        connection.Open();
                        lastValue = Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return lastValue;
        }
    }
}

