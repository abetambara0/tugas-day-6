﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using tugas_day_6.Data;
using tugas_day_6.DTOs;
using tugas_day_6.Model;

namespace taskDay7.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddUserWithTaskController : ControllerBase
    {
        private readonly UserData _userData;
        private readonly TaskData _taskData;

        public AddUserWithTaskController(UserData userData, TaskData taskData)
        {
            _userData = userData;
            _taskData = taskData;
        }

        [HttpGet("GetUserWithTask")]
        public ActionResult GetAll(string? name)
        {
            try
            {
                List<User> users = new List<User>();
                List<UserWithTaskDto> output = new List<UserWithTaskDto>();

                if (name == null)
                {
                    users = _userData.GetAll();
                }
                else users = _userData.GetUsersByName(name);

                foreach (User user in users)
                {
                    List<TaskOutputDto> taskOutputDto = _taskData.GetByUserId(user.pk_user_id);

                    output.Add(new UserWithTaskDto
                    {
                        tasks = taskOutputDto,
                        pk_user_id = user.pk_user_id,
                        name = user.name
                    });
                }
                return Ok(output);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        [HttpPost("AddUserWithTask")]
        public IActionResult Post([FromBody] AddDto addDto)
        {
            try
            {
                User user = new User
                {
                    name = addDto.name
                };

                int insertedId = _userData.Insert(user);

                if (insertedId > 0)
                {
                    foreach (TaskInputDto taskInputDto in addDto.tasks)
                    {
                        UserTask task = new UserTask
                        {
                            task_detail = taskInputDto.task_detail,
                            fk_user_id = insertedId
                        };
                        _taskData.Insert(task);
                    }
                    return StatusCode(201, "Data Succesfully Inserted");

                }
                else
                {
                    return StatusCode(500, "Data Not Inserted");
                }

            }

            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
